# Inisialisasi Library
import telegram  # Import Library Telegram
import cv2      # Import library OpenCV
import numpy    # Import Numpy (Mengolah data array)
# Import OS -> Untuk berinteraksi dengan sistem operasi(Mengambil path file atau folder)
import os
import time     # Import time -> untuk mengambil waktu perangkat
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(20, GPIO.OUT)
GPIO.setup(21, GPIO.OUT)
GPIO.output(21, GPIO.HIGH)
GPIO.output(20, GPIO.HIGH)
time.sleep(1)
GPIO.output(21, GPIO.LOW)
GPIO.output(20, GPIO.LOW)
start_time = time.time()  # inisialisasi waktu sekarang
api_key = '5309310734:AAFMSnf4Jb2pin3QXqDYb7JNZ--l1aVJJWA'  # Api Key Telegram Bot
user_id = '1127974367'  # ID User akun telegram
bot = telegram.Bot(token=api_key)
# Menggunakan Algoritma FisherFace
recognizer = cv2.face.FisherFaceRecognizer_create()
pathu = os.getcwd()  # menampilkan direktori file fisherface.ipynb
nama = ""  # biarkan kosong
waktu_deteksi = int(5)  # ubah sesuai waktu yang ingin di tentukan (Detik)
haarcascade = cv2.CascadeClassifier(pathu+'/haarcascade_frontalface_alt2.xml')  # library citra wajah
(images, labels, dir, names, id) = ([], [], [], {}, 0)
for (subdirs, dirs, files) in os.walk(pathu+"/crop/"):
    for subdir in dirs:
        names[id] = subdir
        subjectpath = os.path.join(pathu+"/crop/", subdir)
        for filename in os.listdir(subjectpath):
            path = subjectpath + "/" + filename
            images.append(path)
            labels.append(int(id))
        id += 1
labels = numpy.array(names)

cam = cv2.VideoCapture(0)
confidence = 0
l=0
ids=''
id = 0
# cam = cv2.VideoCapture(pathu+"/video/ElsaGakPakeKacamata/ElsaGakPakeKacamata.mp4")
while True:
    # font = cv2.FONT_HERSHEY_SIMPLEX  # Jenis Font yang ingin di pakai
    recognizer.read(pathu+"/trainer_fisherface.yml")  # Membaca file Training
    ret, img = cam.read()
    # Mengubah gambar dari warna menjadi abu-abu
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # Akurasi dalam mendektesi wajah dalam gambar
    faces = haarcascade.detectMultiScale(gray, 1.3, 5)
    b = time.time() - start_time  # Mnghitung waktu sekarang
    if (numpy.array(faces).any()):
        for(x, y, w, h) in faces:
            faces = gray[y:y + h, x:x + w]
            resize1 = cv2.resize(faces, (150, 150))
            id, confidence = recognizer.predict(resize1)
            print(names[id])
            if (id>=len(names)):
                ids = 'unknown'
            else:
                ids = names[id]
                recog = round(100 - confidence)
                if(ids != 'unknown'):
                    if (ids == nama):
                        GPIO.output(21, GPIO.HIGH)
                        GPIO.output(20, GPIO.LOW)
                        if b > waktu_deteksi:
                            GPIO.output(21, GPIO.HIGH)
                            GPIO.output(20, GPIO.HIGH)
                            # Mengirim pesan ke telegram bot
                            bot.send_message(chat_id=user_id, text='Wajah Terdektesi '+str(ids))
                            time.sleep(2)
                            start_time = time.time()
                    else:
                        GPIO.output(21, GPIO.LOW)
                        GPIO.output(20, GPIO.HIGH)
                        start_time = time.time()  # Reset Waktu menjadi 0
                    nama = ids
                else:
                    ids = 'unknown'
                    start_time = time.time()
    else:
        ids = 'unknown'
        GPIO.output(21, GPIO.LOW)
        GPIO.output(20, GPIO.HIGH)
        start_time = time.time()
    print(ids + str(b))
    k = cv2.waitKey(10) & 0xff
    if k == 27:
        break
# cam.release()
cv2.destroyAllWindows()
