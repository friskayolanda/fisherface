import cv2
import os
import json
import numpy    # Import Numpy (Mengolah data array)
import shutil
from flask import Flask, request
app = Flask(__name__)
pathu = os.getcwd()
print(pathu)
recognizer = cv2.face.FisherFaceRecognizer_create()
deteksiWajah = cv2.CascadeClassifier(pathu+'/haarcascade_frontalface_alt2.xml')


@app.route("/crop", methods=['POST'])
def crop():
    video = request.files["video"]
    namae = request.form['nama']
    nama = namae.replace(" ", "")
    if not os.path.exists("video/"+nama):
        os.makedirs("video/"+nama)
    video.save(os.path.join(pathu+"/video/"+nama+"/"+nama+".mp4"))

    if not os.path.exists(pathu+"/crop/"+nama):
        os.makedirs(pathu+"/crop/"+nama)
    i = 0
    cap = cv2.VideoCapture(pathu+"/video/"+nama+"/"+nama+".mp4")
    while(cap.isOpened()):
        flag, frame = cap.read()
        if flag == False:
            break
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        abu = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        wajah = deteksiWajah.detectMultiScale(abu, 1.3, 5)
        for (x, y, w, h) in wajah:
            crop = img[y:y+h, x:x+w]
        pathi = pathu+"/crop/"+nama+"/"+nama+str(i)+".jpg"
        wajahAkhir = cv2.cvtColor(crop, cv2.COLOR_BGR2RGB)
        resize = cv2.resize(wajahAkhir, (150, 150))
        cv2.imwrite(pathi, resize)
        i += 1
        # if i >= 100:
        #     break
    result = {
        'Status': "Success"
    }
    return json.dumps(result, indent=4, ensure_ascii=False, sort_keys=True)


@app.route("/training", methods=['GET'])
def training():
    (images, labels, dir, names, id) = ([], [], [], {}, 0)
    for (subdirs, dirs, files) in os.walk(pathu+"/crop/"):
        for subdir in dirs:
            names[id] = subdir
            subjectpath = os.path.join(pathu+"/crop/", subdir)
            for filename in os.listdir(subjectpath):
                path = subjectpath + "/" + filename
                images.append(cv2.imread(path, cv2.IMREAD_GRAYSCALE))
                labels.append(int(id))
            id += 1
    labels = numpy.array(labels)
    recognizer.train(images, labels)
    recognizer.write(pathu+'/trainer_fisherface.yml')
    result = {
        'Status': "Success"
    }
    return json.dumps(result, indent=4, ensure_ascii=False, sort_keys=True)


@app.route("/delete", methods=['POST'])
def delete():
    namae = request.form['nama']
    if (namae):
        shutil.rmtree(pathu+"/video/"+namae, ignore_errors=True)
        shutil.rmtree(pathu+"/crop/"+namae, ignore_errors=True)
        (images, labels, dir, names, id) = ([], [], [], {}, 0)
        for (subdirs, dirs, files) in os.walk(pathu+"/crop/"):
            for subdir in dirs:
                names[id] = subdir
                subjectpath = os.path.join(pathu+"/crop/", subdir)
                for filename in os.listdir(subjectpath):
                    path = subjectpath + "/" + filename
                    images.append(cv2.imread(path, cv2.IMREAD_GRAYSCALE))
                    labels.append(int(id))
                id += 1
        labels = numpy.array(labels)
        recognizer.train(images, labels)
        recognizer.write(pathu+'/trainer_fisherface.yml')
        images = 'Database berhasil di hapus'
    else:
        (images, labels, dir, names, id) = ([], [], [], {}, 0)
        for (subdirs, dirs, files) in os.walk(pathu+"/video/"):
            for subdir in dirs:
                names[id] = subdir
                subjectpath = os.path.join(pathu+"/video/", subdir)
                images.append(subdir)
        print(images)
    result = {
        'data': images
    }
    return json.dumps(result, indent=4, ensure_ascii=False, sort_keys=True)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=False)
